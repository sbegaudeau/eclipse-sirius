/**
 * Copyright (c) 2015 Obeo.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.htm
 */
package org.eclipse.sirius.tests.rcptt.properties.propertiestests;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Test Element</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getStringAttribute
 * <em>String Attribute</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getStringAttributes
 * <em>String Attributes</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getIntAttribute
 * <em>Int Attribute</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getIntAttributes
 * <em>Int Attributes</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#isBooleanAttribute
 * <em>Boolean Attribute</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getBooleanAttributes
 * <em>Boolean Attributes</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getEnumAttribute
 * <em>Enum Attribute</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getEnumAttributes
 * <em>Enum Attributes</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getContainmentReference
 * <em>Containment Reference</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getContainmentReferences
 * <em>Containment References</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getReference
 * <em>Reference</em>}</li>
 * <li>
 * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getReferences
 * <em>References</em>}</li>
 * </ul>
 *
 * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement()
 * @model
 * @generated
 */
public interface TestElement extends EObject {
    /**
     * Returns the value of the '<em><b>String Attribute</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>String Attribute</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>String Attribute</em>' attribute.
     * @see #setStringAttribute(String)
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_StringAttribute()
     * @model
     * @generated
     */
    String getStringAttribute();

    /**
     * Sets the value of the '
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getStringAttribute
     * <em>String Attribute</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     *
     * @param value
     *            the new value of the '<em>String Attribute</em>' attribute.
     * @see #getStringAttribute()
     * @generated
     */
    void setStringAttribute(String value);

    /**
     * Returns the value of the '<em><b>String Attributes</b></em>' attribute
     * list. The list contents are of type {@link java.lang.String}. <!--
     * begin-user-doc -->
     * <p>
     * If the meaning of the '<em>String Attributes</em>' attribute list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>String Attributes</em>' attribute list.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_StringAttributes()
     * @model
     * @generated
     */
    EList<String> getStringAttributes();

    /**
     * Returns the value of the '<em><b>Int Attribute</b></em>' attribute. <!--
     * begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Int Attribute</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Int Attribute</em>' attribute.
     * @see #setIntAttribute(int)
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_IntAttribute()
     * @model
     * @generated
     */
    int getIntAttribute();

    /**
     * Sets the value of the '
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getIntAttribute
     * <em>Int Attribute</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     *
     * @param value
     *            the new value of the '<em>Int Attribute</em>' attribute.
     * @see #getIntAttribute()
     * @generated
     */
    void setIntAttribute(int value);

    /**
     * Returns the value of the '<em><b>Int Attributes</b></em>' attribute list.
     * The list contents are of type {@link java.lang.Integer}. <!--
     * begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Int Attributes</em>' attribute list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Int Attributes</em>' attribute list.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_IntAttributes()
     * @model
     * @generated
     */
    EList<Integer> getIntAttributes();

    /**
     * Returns the value of the '<em><b>Boolean Attribute</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Boolean Attribute</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Boolean Attribute</em>' attribute.
     * @see #setBooleanAttribute(boolean)
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_BooleanAttribute()
     * @model
     * @generated
     */
    boolean isBooleanAttribute();

    /**
     * Sets the value of the '
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#isBooleanAttribute
     * <em>Boolean Attribute</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     *
     * @param value
     *            the new value of the '<em>Boolean Attribute</em>' attribute.
     * @see #isBooleanAttribute()
     * @generated
     */
    void setBooleanAttribute(boolean value);

    /**
     * Returns the value of the '<em><b>Boolean Attributes</b></em>' attribute
     * list. The list contents are of type {@link java.lang.Boolean}. <!--
     * begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Boolean Attributes</em>' attribute list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Boolean Attributes</em>' attribute list.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_BooleanAttributes()
     * @model
     * @generated
     */
    EList<Boolean> getBooleanAttributes();

    /**
     * Returns the value of the '<em><b>Enum Attribute</b></em>' attribute. The
     * literals are from the enumeration
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestEnum}
     * . <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Enum Attribute</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Enum Attribute</em>' attribute.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestEnum
     * @see #setEnumAttribute(TestEnum)
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_EnumAttribute()
     * @model
     * @generated
     */
    TestEnum getEnumAttribute();

    /**
     * Sets the value of the '
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getEnumAttribute
     * <em>Enum Attribute</em>}' attribute. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     *
     * @param value
     *            the new value of the '<em>Enum Attribute</em>' attribute.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestEnum
     * @see #getEnumAttribute()
     * @generated
     */
    void setEnumAttribute(TestEnum value);

    /**
     * Returns the value of the '<em><b>Enum Attributes</b></em>' attribute
     * list. The list contents are of type
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestEnum}
     * . The literals are from the enumeration
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestEnum}
     * . <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Enum Attributes</em>' attribute list isn't
     * clear, there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Enum Attributes</em>' attribute list.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestEnum
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_EnumAttributes()
     * @model
     * @generated
     */
    EList<TestEnum> getEnumAttributes();

    /**
     * Returns the value of the '<em><b>Containment Reference</b></em>'
     * containment reference. <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Containment Reference</em>' containment
     * reference isn't clear, there really should be more of a description
     * here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Containment Reference</em>' containment
     *         reference.
     * @see #setContainmentReference(TestElement)
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_ContainmentReference()
     * @model containment="true"
     * @generated
     */
    TestElement getContainmentReference();

    /**
     * Sets the value of the '
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getContainmentReference
     * <em>Containment Reference</em>}' containment reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @param value
     *            the new value of the '<em>Containment Reference</em>'
     *            containment reference.
     * @see #getContainmentReference()
     * @generated
     */
    void setContainmentReference(TestElement value);

    /**
     * Returns the value of the '<em><b>Containment References</b></em>'
     * containment reference list. The list contents are of type
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement}
     * . <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Containment References</em>' containment
     * reference list isn't clear, there really should be more of a description
     * here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Containment References</em>' containment
     *         reference list.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_ContainmentReferences()
     * @model containment="true"
     * @generated
     */
    EList<TestElement> getContainmentReferences();

    /**
     * Returns the value of the '<em><b>Reference</b></em>' reference. <!--
     * begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reference</em>' reference isn't clear, there
     * really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>Reference</em>' reference.
     * @see #setReference(TestElement)
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_Reference()
     * @model
     * @generated
     */
    TestElement getReference();

    /**
     * Sets the value of the '
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement#getReference
     * <em>Reference</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     *
     * @param value
     *            the new value of the '<em>Reference</em>' reference.
     * @see #getReference()
     * @generated
     */
    void setReference(TestElement value);

    /**
     * Returns the value of the '<em><b>References</b></em>' reference list. The
     * list contents are of type
     * {@link org.eclipse.sirius.tests.rcptt.properties.propertiestests.TestElement}
     * . <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>References</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     *
     * @return the value of the '<em>References</em>' reference list.
     * @see org.eclipse.sirius.tests.rcptt.properties.propertiestests.PropertiestestsPackage#getTestElement_References()
     * @model
     * @generated
     */
    EList<TestElement> getReferences();

} // TestElement
